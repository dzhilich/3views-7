//
//  fullNameViewController.swift
//  3view
//
//  Created by Dmytro Zhylich on 29.05.2021.
//

import UIKit

class fullNameViewController: UIViewController {

    @IBOutlet weak var fNameLabel: UILabel!
    @IBOutlet weak var lNameLabel: UILabel!
    
    var fullName: String?
    var firstName: String?
    var lastName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let fName = firstName {
            fNameLabel.text = fName
        } else {
            fNameLabel.text = ""
        }
        if let lName = lastName {
            lNameLabel.text = lName
        } else {
            fNameLabel.text = ""
        }
        
        fullName = "\(fNameLabel.text!) \(lNameLabel.text!)"
    }
    
    @IBAction func btnSave(_ sender: UIButton) {
        // navigationController?.popToRootViewController(animated: true)
        // navigationController?.popToViewController(firstNameVC, animated: true)
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let navC = storyboard.instantiateViewController(withIdentifier: "firstNameVC") as? UINavigationController,
              let vc = navC.viewControllers.first as? ViewController else { return }
        vc.fullName = fullName
        navigationController?.popToViewController(vc, animated: true)
        
        // navigationController?.pushViewController(vc, animated: true)
        // self.present(navC, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: segue)
        if segue.identifier == "toFullNameVC", let fullNameVC =
        segue.destination as? fullNameViewController {
            fullNameVC.firstName = firstName
            fullNameVC.lastName = lastName
        }
    }
    

}
