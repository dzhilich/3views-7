//
//  ViewController.swift
//  3view
//
//  Created by Dmytro Zhylich on 18.05.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var fullName: String?
    var firstName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // guard let name = fullName else { return }
        
        if let name = fullName {
            fullNameLabel.text = name
            fullNameLabel.isHidden = false
        } else {
            fullNameLabel.isHidden = true
        }
    }

    @IBAction func nextBtn(_ sender: UIButton) {
        firstName = textField.text
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: segue)
        
        if segue.identifier == "toLastNameVC", let lastNameVC =
        segue.destination as? lastNameViewController {
            lastNameVC.tmpFirstName = firstName
        }
    }
    
}

