//
//  lastNameViewController.swift
//  3view
//
//  Created by Dmytro Zhylich on 19.05.2021.
//

import UIKit

class lastNameViewController: UIViewController {

    var lastName: String?
    var tmpFirstName: String?
    
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func nextBtn(_ sender: UIButton) {
        lastName = textField.text
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: segue)
        
        if segue.identifier == "toFullNameVC", let fullNameVC =
        segue.destination as? fullNameViewController {
            fullNameVC.firstName = tmpFirstName
            fullNameVC.lastName = lastName
        }
    }
    
}
